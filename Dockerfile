FROM ubuntu:xenial
MAINTAINER Nguyễn Hồng Quân <ng.hong.quan@gmail.com>

# Note: This is to build Python for console application, so we don't include Tk, which is for GUI app development.

# Use Viet Nam mirror for faster download of x86_64 packages
RUN sed -i -- 's/archive.ubuntu.com/opensource.xtdv.net/g' /etc/apt/sources.list && sed -i -- 's/deb /deb \[arch=amd64,i386\] /g' /etc/apt/sources.list && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales
# Generate locale to get rid of warning about locale.
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
# Install toolchain and library headers for building. I install GCC version 5, which matches the one on my BeagleBone.
# Note: If you want to include Tk, install tk-dev as well
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y sudo vim build-essential binutils-arm-linux-gnueabihf gcc-5-arm-linux-gnueabihf g++-5-arm-linux-gnueabihf cpp-5-arm-linux-gnueabihf llvm-3.9 && apt-get install -y libssl-dev libbz2-dev liblzma-dev libreadline-dev libsqlite3-dev libncurses5-dev libgdbm-dev libexpat-dev && \
# Due to a bug in llvm package that llvm-profdata is not exposed. It is needed for optimizing Python
ln -s /usr/lib/llvm-3.9/bin/llvm-profdata /usr/local/bin/llvm-profdata
# Add repo to install armhf packages. I use Taiwan mirror for faster download in Viet Nam.
RUN echo "deb [arch=armhf] http://free.nchc.org.tw/ubuntu-ports xenial main universe multiverse\ndeb [arch=armhf] http://free.nchc.org.tw/ubuntu-ports xenial-updates main universe multiverse" > /etc/apt/sources.list.d/arm-repo.list && \
    dpkg --add-architecture armhf && apt-get update && \
    apt-get install -y libssl-dev:armhf libbz2-dev:armhf liblzma-dev:armhf libreadline-dev:armhf libsqlite3-dev:armhf libncurses5-dev:armhf libgdbm-dev:armhf libexpat-dev:armhf
# Add normal user quan. Change to any name you like. Also grant this user "sudo" right and don't require password to execute sudo.
RUN adduser --quiet --disabled-password quan && echo "quan:123456" | chpasswd && adduser quan sudo && echo "quan ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/quan
USER quan
